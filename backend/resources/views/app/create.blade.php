<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>New app</h1>
                    <form action="#!" method="POST" id="create-app-form">
                        <div>
                            <label>Name</label>
                            <input type="text" size="50" name="name" class="form-control"/>
                        </div>

                        <div>
                            <label>Callback url</label>
                            <input type="text" size="50" name="redirect" class="form-control"/>
                        </div>

                        <x-primary-button class="mt-3" id="create-app">
                            Create new app
                        </x-primary-button>
                    </form>

                    <div id="result" class="d-none">
                        <p>Client id: <span id="client-id"></span></p>
                        <p>Client secret: <span id="client-secret"></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script>
            // Setting up ajax
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //Post request
            $("#create-app-form").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: '{{ url('oauth/clients') }}',
                    method: 'POST',
                    data: {
                        name: $("[name='name']").val(),
                        redirect: $("[name='redirect']").val()
                    },
                    success: function(res) {
                        $("#result").removeClass('d-none')
                        $("#client-id").text(res.id),
                        $("#client-secret").text(res.secret)
                    },
                    error: function(err) {
                        console.log(err)
                    }
                })
            })
        </script>
    @endpush
</x-app-layout>
