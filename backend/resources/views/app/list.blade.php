<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2>Clients</h2>

                    {{-- Table here --}}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Client Id</th>
                                    <th>Name</th>
                                    <th>Client Secret</th>
                                    <th>Redirect</th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script>
            $.ajax({
                method: 'GET',
                url: '{{ url('oauth/clients') }}',
                success: function(res) {
                    $("tbody").append(
                        res.map(item => {
                            return `<tr>
                                <td>${item.id}</td>
                                <td>${item.name}</td>
                                <td>${item.secret}</td>
                                <td>${item.redirect}</td>
                            </tr>`
                        })
                    )
                },
                error: function(err) {
                    console.log(err)
                }
            })
        </script>
    @endpush
</x-app-layout>
