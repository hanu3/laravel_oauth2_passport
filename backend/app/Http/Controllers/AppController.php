<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Passport;

class AppController extends Controller
{
    public function list()
    {
        return view('app.list');
    }
}
