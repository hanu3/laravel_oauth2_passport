import React from 'react'
import { app } from './config/app'
import axios from 'axios'
import querystring from 'query-string'
import { useLocation } from 'react-router-dom'

const Callback = () => {

  const location = useLocation()

  React.useEffect(() => {
    const code = querystring.parse(location.search).code

    const fetchData = async () => {
      const res = await axios.post('http://localhost:8000/oauth/token', {
        'grant_type': 'authorization_code',
        'client_id': app.CLIENT_ID,
        'client_secret': app.CLIENT_SECRET,
        'redirect_uri': app.CALLBACK_URL,
        'code': code,
      })

      localStorage.setItem('token', JSON.stringify(res.data))
    }

    fetchData()

  }, [])

  return (
    <p>Callback</p>
  )
}

export default Callback