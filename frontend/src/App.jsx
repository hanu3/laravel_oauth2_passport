import { Navigate, RouterProvider, createBrowserRouter } from 'react-router-dom'

import Callback from './Callback';
import Login from './Login';

const router = createBrowserRouter([
  {
    path: "/login",
    element: <Login />
  },
  {
    path: '/callback',
    element: <Callback />
  },
  {
    path: "/",
    element: <Navigate to="/login" />
  }
]);

function App() {
  return (
    <div className="App">
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
}

export default App;
