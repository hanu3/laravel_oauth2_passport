import { app } from './config/app'
import qs from 'query-string'

const Login = () => {

  const buildUrl = () => {
    const querystrings = qs.stringify({
      'client_id': app.CLIENT_ID,
      'redirect_uri': app.CALLBACK_URL,
      'response_type': 'code',
      'scope': '',
      'state': app.CLIENT_SECRET,
    })

    return "http://localhost:8000/oauth/authorize?" + querystrings
  }

  return (
    <a href={buildUrl()}>Login with laravel backend</a>
  )
}

export default Login